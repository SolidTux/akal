# akal

Scrape the website of the Abfallkalender of the city of Karlsruhe and export the data as iCalendar file

## Installation

* Install [Rust](https://rustup.rs)
* Run `cargo install --path . -f`

## Usage

```
USAGE:
    akal <street> <number> <output>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

ARGS:
    <street>    Street name
    <number>    House number
    <output>    Output filename
```
