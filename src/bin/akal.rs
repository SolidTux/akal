use std::{error::Error, fs::File, io::Write, path::PathBuf};

use structopt::StructOpt;

#[derive(StructOpt)]
#[structopt(about, author)]
struct Opt {
    /// Street name
    street: String,
    /// House number
    number: String,
    /// Output filename
    output: PathBuf,
}

fn main() -> Result<(), Box<dyn Error>> {
    let opt = Opt::from_args();

    let mut output = File::create(opt.output)?;
    write!(output, "{}", akal::download(&opt.street, &opt.number)?)?;

    Ok(())
}
