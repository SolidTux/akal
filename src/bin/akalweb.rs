use std::collections::HashMap;

use warp::Filter;

#[tokio::main]
async fn main() {
    let hello = warp::get()
        .and(warp::query::<HashMap<String, String>>())
        .and_then(|p: HashMap<String, String>| async move {
            match p
                .get("street")
                .and_then(|s| p.get("number").map(|n| (s, n)))
            {
                Some((n, s)) => match akal::download(n, s) {
                    Ok(res) => Ok(res),
                    Err(_) => Err(warp::reject()),
                },
                None => Err(warp::reject()),
            }
        });

    warp::serve(hello).run(([0, 0, 0, 0], 2525)).await;
}
