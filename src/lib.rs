use std::error::Error;

use regex::Regex;
use reqwest::Url;
use scraper::{Html, Selector};

pub fn download(street: &str, number: &str) -> Result<String, Box<dyn Error>> {
    let mut res = String::new();

    let url = Url::parse_with_params(
        "https://web6.karlsruhe.de/service/abfall/akal/akal.php",
        &[
            ("hausnr", number),
            ("strasse", &street.to_ascii_uppercase()),
        ],
    )?;

    let content = reqwest::blocking::get(url)?.text()?;
    let document = Html::parse_document(&content);

    let row_selector = Selector::parse(".row").unwrap();
    let cell_selector = Selector::parse(".row > div").unwrap();
    let date_re = Regex::new(r"(\d{2}).(\d{2})\.(\d{4})")?;

    res += "BEGIN:VCALENDAR\n";

    for tr in document.select(&row_selector) {
        let cells: Vec<String> = tr.select(&cell_selector).map(|x| x.inner_html()).collect();
        if cells.len() != 3 {
            continue;
        }
        let name = cells[1].split(',').next().unwrap();
        for capture in date_re.captures_iter(&cells[2]) {
            let day = capture.get(1).unwrap().as_str().to_string();
            let month = capture.get(2).unwrap().as_str().to_string();
            let year = capture.get(3).unwrap().as_str().to_string();

            res += "BEGIN:VEVENT\n";
            res += &format!("SUMMARY:{}\n", name);
            res += &format!("DTSTART;VALUE=DATE:{}{}{}\n", year, month, day);
            res += "END:VEVENT\n";
        }
    }

    res += "END:VCALENDAR\n";

    Ok(res)
}
